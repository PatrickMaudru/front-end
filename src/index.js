import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

// Tell React where to render the Application,
// Element with id "root" has to be available in the static HTML file under public/
ReactDOM.render(<App />, document.getElementById('root'));
