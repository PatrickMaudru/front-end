import React, { Component } from 'react';
import './App.css';
import List from "./components/list";

class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <List />
        );
    }
};

export default App;
