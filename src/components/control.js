import './control.css';
import React from "react";


class Control extends React.Component{
    constructor(props){
        super(props);
    }

    render() {
        return(
            <div className={"control"}>
                <input type={"text"} placeholder={"Search..."}/>
                <button onClick={this.props.handleAdd}>
                    <span className="material-icons">
                        add
                    </span>
                </button>
                <button onClick={this.props.handleDelete}>
                    <span className="material-icons">
                        delete
                    </span>
                </button>
                <button onClick={this.props.handleExport}>
                    <span className="material-icons">
                        import_export
                    </span>
                </button>
            </div>
        );
    }
};

export default Control;