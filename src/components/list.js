import './list.css';
import React, {useEffect} from 'react';
import '../../src/App.css';
import Control from "./control";
import axios from 'axios';
import { saveAs } from 'file-saver';



class List extends React.Component {
    constructor(props) {super(props);
        this.handleList = this.handleList.bind(this);
        this.handleSelection = this.handleSelection.bind(this);
        this.handleValue = this.handleValue.bind(this);
        this.state = { list: [], item: 0, counter: 0 };
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleDate = this.handleDate.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleExport = this.handleExport.bind(this);
        this.handleSelection = this.handleSelection.bind(this);

        let list = [];
        axios.get("http://localhost:8080/api/item/allItems").then((result)=> {
            console.log(result.data);
            list = result.data
        }).catch((error)  => {
            console.log(error);
        });

        this.state = {
            list: list,
            item: null,
            counter: 0
        };
    }

    handleList() {

    }

    handleSelection(e) {
        this.setState({
            item: e.currentTarget.dataset.id
        });
    }

    handleValue(value) {

    }

    handleTitle(title) {
        if (!title) {
            title = "Neue Notiz";
        };
        return title;
    }

    handleTitleChange(title, id) {
        this.state.list[id].title = title;
        this.state.list[id].date = this.handleDateChange();
        this.forceUpdate()
    }

    handleDescription(description) {
        if (!description) {
            description = "Kein zusätzlicher Inhalt";
        };
        return description;
    }

    handleDescriptionChange(description, id) {
        this.state.list[id].description = description;
        this.state.list[id].date = this.handleDateChange();
        this.forceUpdate()
    }

    handleDate(date) {
        return ("12.12.2020");
    }

    handleDateChange() {
        var d = new Date();
        let date = d.getDate();
        let month = d.getMonth();
        let year = d.getFullYear();
        if(date <= 9){
            date = "0" + date;
        };

        if(month <= 9){
            month = "0" + month;
        };
        return date + "." + month + "." + year;
    }

    handleAdd() {
        const newItem = {
            id: this.state.counter,
            title: this.handleTitle(),
            description: this.handleDescription(),
            date: this.handleDateChange()
        };

        axios.post("http://localhost:8080/api/item/addItem", newItem).then((result)=> {
            console.log(result.data);
        }).catch((error)  => {
            console.log(error);
        });

        const newList = [...this.state.list, newItem];

        this.setState({
            list: newList,
            counter: this.state.counter + 1
        });
    }

    handleDelete() {
        let array = this.state.list;
        let index = this.state.item;
        if (index !== null) {

            axios.post("http://localhost:8080/api/item/DeleteItem", array[index]).then((result)=> {
                console.log(result.data);
            }).catch((error)  => {
                console.log(error);
            });

            delete array[index];

            this.setState(
                {
                    list: array.filter(list => list.id !== null && list.id !== undefined),
                    item: this.state.list.length - 1,
                    counter: this.state.list.length
                });
            let list = this.state.list;
            if (list.length === 0) {
                this.setState(
                    {
                        item: null,
                        counter: 0
                    });
            }
        } else {
            alert("You should select an item to delete!");
        }
    }

    handleExport(id) {
        let array = this.state.list;
        let index = this.state.item;
        console.log(array[index].title)
        var blob = new Blob([array[index].title, array[index].description], {type: "text/plain;charset=utf-8"});
        saveAs(blob, array[index].title + ".txt");
    }

    render() {
        const list = this.state.list;
        let ss = this.state.list[this.state.item];
        let title, description, date, id;
        if (ss) {
            id = ss.id
            title = ss.title;
            description = ss.description;
            date = ss.date
        }
        const items = list.map(list =>
            <li className={"item"} key={list.id} data-id={list.id} onClick={this.handleSelection.bind(this)}>
                <h1 className={"title"}>{list.title}</h1>
                <p className={"date"}>{list.date}</p>
                <p className={"description"}>{list.description}</p>
            </li>
        );
        return (
            <>
                <div className={"app"}>
                    <div className={"navigation"}>
                        <Control
                            handleAdd={this.handleAdd}
                            handleDelete={this.handleDelete}
                            handleExport={this.handleExport}
                        />
                        <ul className={"list"}>
                            {items}
                        </ul>
                    </div>
                    <div className={"content"}>
                        {this.state.item != null &&
                        <>
                            <p className={"date"}>{date}</p>
                            <textarea className={"title"} placeholder={"Neue Notiz"} value={title} id={id} onChange={(e) => { this.handleTitleChange(e.target.value, e.target.id) }} >
                                </textarea>
                            <textarea className={"description"} placeholder={"Kein zusätzlicher Inhalt"} value={description} id={id} onChange={(e) => { this.handleDescriptionChange(e.target.value, e.target.id) }}>
                                </textarea>
                        </>
                        }
                    </div>
                </div>
            </>
        );
    }
};

export default List;